use std::{
    fs::{self, File},
    io::{self, BufReader, BufWriter, Read, Write},
    path::PathBuf,
};

use clap::{Parser, Subcommand};
use flate2::{
    read::{ZlibDecoder, ZlibEncoder},
    Compression,
};
use sha1::{Digest, Sha1};

#[derive(Parser)]
struct Args {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Create an empty Git repository or reinitialize an existing one
    Init,
    /// Provide contents or details of repository objects
    CatFile {
        /// Pretty-print the contents of the object based on its type
        #[arg(short)]
        pretty: bool,
        /// The name of the object to show
        name: String,
    },
    /// Compute object ID and optionally create an object from a file
    HashObject {
        /// Actually write the object into the object database
        #[arg(short)]
        write: bool,
        /// Path to the file to hash
        path: PathBuf,
    },
}

fn init() -> io::Result<()> {
    fs::create_dir(".git")?;
    fs::create_dir(".git/objects")?;
    fs::create_dir(".git/refs")?;
    fs::write(".git/HEAD", "ref: refs/heads/master\n")?;

    println!("Initialized git directory");
    Ok(())
}

fn cat_file(_pretty: bool, name: &str) -> anyhow::Result<()> {
    let (dirname, filename) = name.split_at(2);
    let object = File::open(format!(".git/objects/{}/{}", dirname, filename))?;
    let reader = BufReader::new(object);
    let mut decoder = ZlibDecoder::new(reader);
    let mut buf = Vec::new();

    let _len = decoder.read_to_end(&mut buf)?;
    let pos = buf
        .iter()
        .position(|&x| x == 0)
        .expect("No null byte found");
    let (_header, contents) = buf.split_at(pos);

    print!("{}", std::str::from_utf8(&contents[1..])?);
    Ok(())
}

fn hash_object(write: bool, path: PathBuf) -> anyhow::Result<()> {
    let content = fs::read(path)?;
    let header = format!("blob {}\0", content.len()).into_bytes();
    let mut store = header;
    store.extend(content);

    let mut hasher = Sha1::new();
    hasher.update(&store);
    let hash = hasher.finalize();
    let hash_str = hex::encode(hash);

    println!("{hash_str}");

    if write {
        let mut encoder = ZlibEncoder::new(&store[..], Compression::default());
        let mut buf = Vec::new();
        let _len = encoder.read_to_end(&mut buf)?;
        let (dirname, filename) = hash_str.split_at(2);
        let object = File::open(format!(".git/objects/{}/{}", dirname, filename))?;
        let mut writer = BufWriter::new(object);

        writer.write_all(&buf)?;
    }

    Ok(())
}

fn main() -> anyhow::Result<()> {
    let args = Args::parse();

    match args.command {
        Commands::Init => init()?,
        Commands::CatFile { pretty, name } => cat_file(pretty, &name)?,
        Commands::HashObject { write, path } => hash_object(write, path)?,
    }

    Ok(())
}
